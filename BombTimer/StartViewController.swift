//
//  StartViewController.swift
//  BombTimer
//
//  Created by michal on 05/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var messageText: UITextField!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var otherTime: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        messageText.resignFirstResponder()
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        performSegue(withIdentifier: "goBomb", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goBomb" {
            let destinationVC = segue.destination as! ViewController
            
            destinationVC.message = messageText.text!
            if otherTime.isOn {
                destinationVC.totalTime = Int(timePicker.countDownDuration)
            }
        }
    }
    
}
