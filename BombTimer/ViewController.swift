//
//  ViewController.swift
//  BombTimer
//
//  Created by michal on 04/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var messageText: UITextView!
    
    var message: String?
    
    var countdownTimer: Timer!
    var totalTime = 6
    var timerRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageText.text = message
        startTimer()
    }
    
    @objc func startTimer() {
        if timerRunning == false {
            timerRunning = true
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        timerRunning = false
        self.view.backgroundColor = UIColor.blue
        timerLabel.textColor = UIColor.white
        Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(vibrate), userInfo: nil, repeats: true)
    }

    @objc func vibrate() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
}
